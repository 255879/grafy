#include "Bellman.hpp"
#include "Dijkstra.hpp"
#include "matrix.hpp"
#include "list.hpp"


int main()
{

	Matrix GraphMatrix;
	List GraphList;
	auto AllTimeMB = 0, AllTimeLB = 0;
	auto AllTimeMD = 0, AllTimeLD = 0;
	int Edg[5] = { 6, 50, 100, 250, 500 };
	double Dens[4] = { 0.25, 0.5, 0.75, 1 };

	int a = 0, b = 0, c = 0, d = 0;
	do
	{
		std::cout << "Test(random, time) or Fun(plik)" << std::endl << "1.Test\n2.Fun\n9.Exit\n" << std::endl;
		std::cout << "Opcja:" ;
		std::cin >> a;
		switch (a)
		{
		case 1: // кнопка D
			std::cout << "Bellman" << std::endl;
			for (int i = 0; i < 0; i++)
			{
				for (int j = 0; j < 4; j++)
				{
					for (int z = 0; z < 2; z++)
					{
						GraphMatrix.RandomGraph(Edg[i], Dens[j]);
						auto beginMB = std::chrono::steady_clock::now();
						MatrixBellmanFord(GraphMatrix);
						auto endMB = std::chrono::steady_clock::now();
						auto timeMB = std::chrono::duration<double, std::micro>(endMB - beginMB).count();
						AllTimeMB += timeMB;

						GraphList.RandomGraph(Edg[i], Dens[j]);
						auto beginLB = std::chrono::steady_clock::now();
						ListBellmanFord(GraphList);
						auto endLB = std::chrono::steady_clock::now();
						auto timeLB = std::chrono::duration<double, std::micro>(endLB - beginLB).count();
						AllTimeLB += timeLB;
					}
					std::cout << Edg[i] << " " << Dens[j] << std::endl;
					std::cout << "Matrix: " << AllTimeMB / 2 << " us" << std::endl;
					std::cout << "List: " << AllTimeLB / 2 << " us" << std::endl;
					AllTimeLB = 0; AllTimeMB = 0;
				}
			}
			std::cout << "Dijkstra" << std::endl;
			for (int i = 0; i < 5; i++)
			{
				for (int j = 0; j < 4; j++)
				{
					for (int z = 0; z < 2; z++)
					{
						GraphMatrix.RandomGraph(Edg[i], Dens[j]);
						auto beginMD = std::chrono::steady_clock::now();
						MatrixDijkstra(GraphMatrix);
						auto endMD = std::chrono::steady_clock::now();
						auto timeMD = std::chrono::duration<double, std::micro>(endMD - beginMD).count();
						AllTimeMD += timeMD;

						GraphList.RandomGraph(Edg[i], Dens[j]);
						auto beginLD = std::chrono::steady_clock::now();
						ListDijkstra(GraphList);
						auto endLD = std::chrono::steady_clock::now();
						auto timeLD = std::chrono::duration<double, std::micro>(endLD - beginLD).count();
						AllTimeLD += timeLD;
					}
					std::cout << Edg[i] << " " << Dens[j] << std::endl;
					std::cout << "Matrix: " << AllTimeMD / 2 << " us" << std::endl;
					std::cout << "List: " << AllTimeLD / 2 << " us" << std::endl;
					AllTimeLD = 0; AllTimeMD = 0;
				}
			}
			break;
		case 2:
				std::cout << std::endl;
				std::cout << "1.Matrix\n2.List\n";
				std::cin >> b;
				if (b == 1)
				{
					do {
						std::cout << "Matrix" << std::endl;
						std::cout << "Wybierz opcję:\n";
						std::cout << "1. ReadFile\n2.Print\n3.AddVerticale\n4.Bellman\n5.Dijkstra\n9.Exit\n";
						std::cin >> c;
						if (c == 1)
						{
							std::cout << "Read file with name Input.txt" << std::endl;
							GraphMatrix.ReadFile();
						}

						else if (c == 2)
						{
							std::cout << "Print graph" << std::endl;
							GraphMatrix.GraphPrint();
						}
						else if (c == 3)
						{
							std::cout << "Add graph" << std::endl;
							GraphMatrix.GraphAdd();
						}
						else if (c == 4)
						{
							std::cout << "Algorythm Bellman" << std::endl;
							MatrixBellmanFord(GraphMatrix);
						}
						else if (c == 5)
						{
							std::cout << "Algorythm Dijkstra" << std::endl;
							MatrixDijkstra(GraphMatrix);
						}
						else if (c == 9)
						{
							std::cout << "Exit";
						}
					} while (c != 9);
				}
				else if (b == 2)
				{
					do {
					std::cout << "List" << std::endl;
					std::cout << "Wybierz opcję:\n";
					std::cout << "1.ReadFile\n2.Print\n3.AddVerticale\n4.Bellman\n5.Dijkstra\n9.Exit\n";
					std::cin >> d;
						if (d == 1)
						{
							std::cout << "Read file with name Input.txt" << std::endl;
							GraphList.ReadFile();
						}
						else if (d == 2)
						{
							std::cout << "Print graph" << std::endl;
							GraphList.GraphPrint();
						}
						else if (d == 3)
						{
							GraphList.GraphAdd();
							std::cout << "Add graph" << std::endl;
						}
						else if (d == 4)
						{
							std::cout << "Algorythm Bellman" << std::endl;
							ListBellmanFord(GraphList);
						}
						else if (d == 5)
						{
							std::cout << "Algorythm Dijkstra" << std::endl;
							ListDijkstra(GraphList);
						}
						else if (c == 9)
						{
							std::cout << "Exit";
						}
					} while (d != 9);

				}
		case 9:
			std::cout << "Exit";
			break;
		}
		
	} while (a != 9);
}
