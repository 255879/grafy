#include "Bellman.hpp"

void MatrixBellmanFord(Matrix GraphMatrix)
{
	int MaxInt = 2147483647;
	int* PayToGo, * Way;
	int Start = GraphMatrix.getStartPoint();
	int Edg = GraphMatrix.getEdges();
	PayToGo = new int[Edg];
	Way = new int[Edg];
	for (int i = 0; i < Edg; i++)
	{
		Way[i] = -1;
		PayToGo[i] = MaxInt;
	}
	PayToGo[Start] = 0;
	for (int i = 1; i < Edg; i++)
	{
		for (int j = 0; j < Edg; j++)
		{
			for (int z = 0; z < Edg; z++)
			{
				if (PayToGo[j] != MaxInt && (GraphMatrix(z, j) != 0 || GraphMatrix(z, j) != 999) && PayToGo[z] > PayToGo[j] + GraphMatrix(z, j))
				{
					PayToGo[z] = PayToGo[j] + GraphMatrix(z, j);
					Way[z] = j;
				}
			}

		}

	}
	//OnMyWay(PayToGo, Edg, Way);
}

void ListBellmanFord(List GraphList)
{
	OneList* neighb;
	int MaxInt = 2147483647;
	int* PayToGo, * Way;
	int Start = GraphList.getStartPoint();
	int Edg = GraphList.getEdges();
	PayToGo = new int[Edg];
	Way = new int[Edg];
	for (int i = 0; i < Edg; i++)
	{
		Way[i] = -1;
		PayToGo[i] = MaxInt;
	}
	PayToGo[Start] = 0;
	for (int i = 1; i < Edg; i++)
	{
		for (int j = 0; j < Edg; j++)
		{
			for (neighb = GraphList[j]; neighb; neighb = neighb->Next)
			{
				if (PayToGo[j] != MaxInt && PayToGo[neighb->Neighbor] > PayToGo[j] + neighb->Weight)
				{
					PayToGo[neighb->Neighbor] = PayToGo[j] + neighb->Weight;
					Way[neighb->Neighbor] = j;
				}
			}
		}
	}
	//OnMyWay(PayToGo, Edg, Way);
}

void OnMyWay(int* PayToGo, int Edg, int* Way)
{
	int* Show = new int[Edg];
	int n = 0;
	int m;
	std::cout << "Bellman" << std::endl;
	for (int i = 0; i < Edg; i++)
	{
		std::cout << i << " $" << PayToGo[i] << " ";
		for (int j = i; j > -1; j = Way[j]) Show[n++] = j;
		while (n) std::cout << Show[--n] << " ";
		std::cout << std::endl;
	}
	delete[]Show;
}
