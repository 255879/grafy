#include "Dijkstra.hpp"

int MinDist(int* Distance, bool* Tset, int Edge)
{
	int MaxInt = 2147483647;
	int minimum = MaxInt;
	int tmp;

	for (int i = 0; i < Edge; i++)
	{
		if (Tset[i] == false && Distance[i] <= minimum)
		{
			minimum = Distance[i];
			tmp = i;
		}
	}
	return tmp;
}

void MatrixDijkstra(Matrix GraphMatrix)
{
	int* Distance, * Way;
	int Start = GraphMatrix.getStartPoint();
	bool* Tset;
	int MaxInt = 2147483647;
	Way = new int[GraphMatrix.getEdges()];
	Distance = new int[GraphMatrix.getEdges()];
	Tset = new bool[GraphMatrix.getEdges()];
	for (int i = 0; i < GraphMatrix.getEdges(); i++)
	{
		Distance[i] = MaxInt;
		Tset[i] = false;
		Way[i] = -1;
	}

	Distance[Start] = 0;

	for (int i = 0; i < GraphMatrix.getEdges(); i++)
	{
		int m = MinDist(Distance, Tset, GraphMatrix.getEdges());
		Tset[m] = true;
		for (int j = 0; j < GraphMatrix.getEdges(); j++)
		{
			if (!Tset[j] && Distance[m] != MaxInt && Distance[m] + GraphMatrix(m, j) < Distance[j])
			{
				Distance[j] = Distance[m] + GraphMatrix(m, j);
				Way[m] = j;
			}
		}
	}
	//OnMyWayHome(Distance, GraphMatrix.getEdges(), Way);
}

void ListDijkstra(List GraphList)
{
	int* Distance, * Way;
	OneList* neighb;
	int Start = GraphList.getStartPoint();
	bool* Tset;
	int MaxInt = 2147483647;
	Way = new int[GraphList.getEdges()];
	Distance = new int[GraphList.getEdges()];
	Tset = new bool[GraphList.getEdges()];
	for (int i = 0; i < GraphList.getEdges(); i++)
	{
		Distance[i] = MaxInt;
		Tset[i] = false;
		Way[i] = -1;
	}

	Distance[GraphList.getStartPoint()] = 0;

	for (int i = 0; i < GraphList.getEdges(); i++)
	{
		int m = MinDist(Distance, Tset, GraphList.getEdges());
		Tset[m] = true;
		for (neighb = GraphList[m]; neighb; neighb = neighb->Next)
		{
			if (!Tset[neighb->Neighbor] && Distance[m] != MaxInt && Distance[m] + neighb->Weight < Distance[neighb->Neighbor])
			{
				Distance[neighb->Neighbor] = Distance[m] + neighb->Weight;
				Way[neighb->Neighbor] = m;
			}
		}
	}
	//OnMyWayHome(Distance, GraphList.getEdges(), Way);
}
void OnMyWayHome(int* PayToGo, int Edg, int* Way)
{
	int* Show = new int[Edg];
	int n = 0;
	int m;
	std::cout << "Dijkstra" << std::endl;
	for (int i = 0; i < Edg; i++)
	{
		std::cout << i << " $" << PayToGo[i] << " ";
		for (int j = i; j > -1; j = Way[j]) Show[n++] = j;
		while (n) std::cout << Show[--n] << " ";
		std::cout << std::endl;
	}
	delete[]Show;
}
