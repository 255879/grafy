#pragma once
#include "list.hpp"
#include "matrix.hpp"

#include <chrono>
#include <iomanip>

void MatrixDijkstra(Matrix GraphMatrix, std::fstream& File);
void ListDijkstra(List GraphList, std::fstream& File);
void OnMyWayHome(int* PayToGo, int Edg, int* Way, std::fstream& File);
int MinDist(int* Distance, bool* Tset, int Edge);